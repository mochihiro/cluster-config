#!/bin/bash

scp ansible-namespace.yml white-widow@192.168.0.244:/home/white-widow
scp -r white-widow white-widow@192.168.0.244:/home/white-widow/

ssh white-widow@192.168.0.244 "/home/white-widow/run-ansible.sh"
